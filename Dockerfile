ARG BASE_REGISTRY=registry1.dso.mil
ARG BASE_IMAGE=ironbank/redhat/ubi/ubi8
ARG BASE_TAG=8.5
ARG ARTIFACTORY_BASE_VERSION=7.35.2

FROM releases-docker.jfrog.io/jfrog/artifactory-oss:${ARTIFACTORY_BASE_VERSION} AS base

FROM ${BASE_REGISTRY}/${BASE_IMAGE}:${BASE_TAG}

USER root

# Set vars
ENV JF_ARTIFACTORY_USER=artifactory \
    ARTIFACTORY_VERSION=${ARTIFACTORY_VERSION} \
    ARTIFACTORY_BOOTSTRAP=/artifactory_bootstrap \
    JF_PRODUCT_HOME=/opt/jfrog/artifactory \
    JF_PRODUCT_DATA_INTERNAL=/var/opt/jfrog/artifactory \
    RECOMMENDED_MAX_OPEN_FILES=32000 \
    MIN_MAX_OPEN_FILES=10000 \
    RECOMMENDED_MAX_OPEN_PROCESSES=1024 \
    TOMCAT_HOME=/opt/jfrog/artifactory/app/artifactory/tomcat

COPY --from=base /opt/jfrog/artifactory/ /opt/jfrog/artifactory/
COPY --from=base /var/opt/jfrog/artifactory/ /var/opt/jfrog/artifactory/
COPY --from=base /opt/jfrog/artifactory/app/third-party /opt/jfrog/artifactory/app/third-party
COPY ./scripts/entrypoint-artifactory.sh /
COPY ./scripts/* /opt/jfrog/artifactory/app/bin/
COPY info/* /var/opt/jfrog/artifactory/etc/artifactory/info/

# Handle permissions and ownership in ${JF_PRODUCT_HOME}/app
# set artifactory home and data
# set docker as distribution for callhome
# Handle run with custom uid and gid with no volume mounted
# Handle bootstrap directory permissions
RUN groupadd -g 1030 ${JF_ARTIFACTORY_USER} && \
    useradd -d "$JF_PRODUCT_HOME" -u 1030 -g 1030 -m -s /bin/bash ${JF_ARTIFACTORY_USER} && \
    dnf upgrade -y --nodoc && \
    dnf install -y procps-ng net-tools cronie hostname --setopt=tsflags=nodocs &&  \
    dnf clean all && \
    rm -rf /var/cache/dnf && \
    chown -R ${JF_ARTIFACTORY_USER}:${JF_ARTIFACTORY_USER} ${JF_PRODUCT_HOME}  && \
    chmod 777 ${JF_PRODUCT_HOME}/app/run && \
    chmod -R 755 ${JF_PRODUCT_HOME}/app/artifactory/tomcat && \
    chmod -R 777 ${JF_PRODUCT_HOME}/app/artifactory/tomcat/webapps && \
    chmod -R go+w ${JF_PRODUCT_HOME}/app ${JF_PRODUCT_HOME}/app/bin ${JF_PRODUCT_HOME}/app/artifactory/tomcat/conf && \
    chmod +x ${JF_PRODUCT_HOME}/app/bin/* ${JF_PRODUCT_HOME}/app/artifactory/tomcat/bin/* && \
    ln -s ${JF_PRODUCT_DATA_INTERNAL} ${JF_PRODUCT_HOME}/var && \
    chown -R ${JF_ARTIFACTORY_USER}:${JF_ARTIFACTORY_USER} ${JF_PRODUCT_DATA_INTERNAL} /entrypoint-artifactory.sh && \
    chmod +x /entrypoint-artifactory.sh && \
    chown ${JF_ARTIFACTORY_USER}:${JF_ARTIFACTORY_USER} ${JF_PRODUCT_HOME}/app && \
    chmod -R 777 ${JF_PRODUCT_DATA_INTERNAL} && \
    mkdir -p ${ARTIFACTORY_BOOTSTRAP} && \
    chmod -R 766 ${ARTIFACTORY_BOOTSTRAP} && \
    chown -R ${JF_ARTIFACTORY_USER}:${JF_ARTIFACTORY_USER} ${ARTIFACTORY_BOOTSTRAP}

# Expose Router's port
EXPOSE 8081

# The user that will run the container and artifactory
USER ${JF_ARTIFACTORY_USER}

# Default mount for data directory
VOLUME ${JF_PRODUCT_DATA_INTERNAL}

# Change workdir to Artifactory Home
WORKDIR ${JF_PRODUCT_HOME}

HEALTHCHECK --interval=30s --timeout=5s --start-period=2m --retries=5 \
  CMD curl -I -f http://localhost:8082/ui/ || exit 1

# Start the simple standalone mode of Artifactory
ENTRYPOINT ["/entrypoint-artifactory.sh"]